# base-image

Includes tests for verifying the base images for both Automotive and RHEL/Edge initiatives, images are built using the configurations defined in the ci-cd/maniftests project.

## To run the tests locally
`tmt run plans -n . -vvv`

## To run the tests on provision virtual
`tmt run -a -vvv plans -n . provision --how virtual --image https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20211119.0.x86_64.qcow2`

## To run the tests on provision minute
`tmt run -a -vvv plans -n . provision --how minute --image 1MT-CentOS-Stream-9`

All of these are running all plans in current directory, so running this in e.g. smoke/display would just run all plans in smoke/display directory which for now is server.fmf and ui.fmf

## How to check the tests

Every time a test is pushed it gets automatically checked for syntax or format
errors with [ShellCheck](https://github.com/koalaman/shellcheck). It's convenient
to use it locally before to push the code, to save some time and be able to quickly
catch misspellings and silly errors.

To check locally for those errors it's recommended to install `ShellCheck` with
the package manager (e.g. `dnf install ShellCheck`) and to run this line in the
directory of the changed code:

```shell
$ shellcheck -S error *.sh
```

If there are more directories with scripts or libraries, they can be checked
recursively with the following:

```shell
$ find -name '*.sh' -exec shellcheck -S error {} +
```

