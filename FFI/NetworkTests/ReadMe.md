# Freedom From Interference (FFI)

With the ISO 26262 Freedom from interference (FFI) can be described as the absence of cascading failures between two or more elements that could lead to the violation of a safety requirement.
For example: a system with a less critical ASIL level which can not influence a system with a more critical ASIL application. 
The goal is to demonstrate container technologies can be used to isolate applications belonging to different safety domains (ex- ASIL-B and QM).

## To run the tests locally
1. `tmt init`
2. `tmt run -a -vvv`

## To run the tests on provision- virtual:
1.  `tmt init`
2.  `tmt run -a -vvv provision --how virtual --image http://cloud.centos.org/centos/8-stream/x86_64/images/CentOS-Stream-GenericCloud-8-20210603.0.x86_64.qcow2`

## This Test Suite include 2 tests:

Setup for which includes installing podman, pulling the image and then build the image using relevant epel, iputils and sysstat installation.

1. tc01_network_stress_send_large_packets:
Two containers running in a vm confusion and orderly:
Firstly, Orderly pings google.com 10 times in ideal conditions (time noted) and then confusion sends large and unstoppable packets using  `ping -s` while orderly again pings google.com 10 times (while in network stress due to confusion container)time noted again and compared with time taken in ideal conditions.Fail is displayed if time taken is more than 5% while pinging google.com under stress.The metrics such as Packet delay, loss and errors are monitored using ping and sar. For the purpose of collecting data regarding errors google.com is pinged again. Evaluations present are - time delay should not be more than 5% when orderly pings google.com under stress and packet loss should not be more than 0%.


2. tc02_network_stress_ping_storm.sh:
Two containers running in a vm confusion and orderly:
Firstly, Orderly pings google.com 10 times in ideal conditions (time noted) and then confusion creates a ping storm using  `ping -f` while orderly again pings google.com 10 times (while in network stress or ping storm due to confusion container)time noted again and compared with time taken in ideal conditions.Fail is displayed if time taken is more than 5% while pinging google.com under stress.The metrics such as Packet delay, loss and errors are monitored using ping and sar. For the purpose of collecting data regarding errors google.com is pinged again. Evaluations present are - time delay should not be more than 5% when orderly pings google.com under stress and packet loss should not be more than 0%.

## The metrics for these tests are collected using :
1. `podman ps --all` for listing containers.
2. `sar` for network statistics.
3. `ping` for timing and packet loss.