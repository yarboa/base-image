#!/bin/bash
echo "*****************************************ACTIVE CONTAINERS*****************************************"
echo ""
podman ps --all
echo ""
echo "*****************************************CPU UTILIZATION*******************************************"
mpstat -P ALL 1 45 # All CPUs output five groups one second apart
echo ""
#echo "CPU usage is "

#echo $[100-$(vmstat 1 2|tail -1|awk '{print $15}')]

#echo "*****************************************MEMORY ALLOCATION**********************************************"
#cat /proc/meminfo
echo ""
echo "*****************************************memory statistic*************************************"
vmstat -S m
echo ""
echo "*****************************************Shared Resources*************************************************"
free -t -h -w
