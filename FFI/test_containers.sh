#!/bin/bash

set -eu

TYPE="${1:-}"
declare -a CONTAINER_LIMITS

case "$TYPE" in
    "cpu")
        echo "Start test: limit CPU"
	CONTAINER_LIMITS=(--cpu-shares 512 --cpuset-cpus 0)
        echo "Summary: add limit with ${CONTAINER_LIMITS[*]}"
        ;;
    "memory")
        echo "Start test: limit memory"
	CONTAINER_LIMITS=(--memory 512m)
        echo "Summary: add limit with ${CONTAINER_LIMITS[*]}"
        ;;
    *)
        echo "Start test: Basic"
        echo "Summary: default container options"
	;;
esac

echo "Create Orderly container"
podman run -d "${CONTAINER_LIMITS[@]}" --name orderly ubi8-stress

echo "Create Confusion container"
podman run -d "${CONTAINER_LIMITS[@]}" --name confusion ubi8-stress

echo "Monitor the Orderly container health"
podman exec -it orderly watch-it &

echo "Run some interferences in the confusion container"
podman exec -it confusion sh -c "uptime ; stress --cpu 4 --vm-bytes 2G -t 50s ; uptime"

echo "Cleaning up containers"
echo ">>> Remove container orderly"
podman rm -f orderly
echo ">>> Remove container confusion"
podman rm -f confusion

echo "End test"
