# Base image smoke tests

Simple test suite of smoke tests for the base RHIVOS image

## To run the smoke tests locally
`tmt run plans -n /smoke -vvv`

## To run the smoke tests on provision virtual
`tmt run -a -vvv plans -n /smoke provision --how virtual --image https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20211119.0.x86_64.qcow2`

## To run the smoke tests on provision minute
`tmt run -a -vvv plans -n /smoke provision --how minute --image 1MT-CentOS-Stream-9`

## To run specific set of tests inside smoke directory
Just change the name filter for plans to you desired test plan, e.g.:
`tmt run -a -vvv plans -n /smoke/display provision --how virtual --image https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20211119.0.x86_64.qcow2`

## This Test Suite includes multiple smoke tests

Setup for which includes installing required packages (python3, xorg, gdm, neptune3-ui_copr) and configuring a 
xorg dummy video driver. The test also configures an edge user to auto boot this environment.
After these configurations take place, the test utilizes tmt-reboot to obtain the startup timings after being enabled

1. Disk tests - partition type
2. Display tests - Xorg and neptune3-ui running
3. Distro tests - check if we running on CentOS Stream
4. DNF tests - check enabled repositories
5. Ostree tests - check if we are running Ostree image
6. Service tests - check default services are running

